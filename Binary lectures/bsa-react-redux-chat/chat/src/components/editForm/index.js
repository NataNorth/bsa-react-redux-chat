import React from 'react';
import './index.css'
import { connect } from 'react-redux'
import { editMessage } from '../messageList/actions'
import { hidePage, dropCurrentMessageId } from './actions';

class EditForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: '',
            text: ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getEditForm = this.getEditForm.bind(this);
        this.onCancel = this.onCancel.bind(this);
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if(nextProps.messageId !== prevState.id && nextProps.isShown) {
            var currentMessage = nextProps.messages.find(message => message.id === nextProps.messageId);
            console.log('next ' + currentMessage.text + ' prev ' + prevState)
            console.log(prevState)
            if (!prevState.text) {
                return {
                    text: currentMessage.text
                };
            } else {
                return {
                    text: prevState.text
                }
            }
        } else {
            return {
                text: ''
            }
        }
    }

    handleSubmit(e){
        e.preventDefault();
        this.props.editMessage(this.props.messageId, this.state.text);
        this.props.hidePage();
    }

    handleChange(e) {
        e.preventDefault();
        this.setState({text: e.target.value});
    }

    onCancel() {
        this.props.dropCurrentMessageId();
        this.setState({
            id: ''
        })
        console.log('Cancel ' + this.props.messageId);
        this.props.hidePage();
        console.log('Cancel ' + this.props.messageId);
    }

    getEditForm() {
        let data = this.state;
        return (
            <div className = "editForm">
                <form 
                    onSubmit={this.handleSubmit} >
                        <textarea
                        id='editFormTextarea'
                        onChange={this.handleChange}
                        value={data.text} />
                    <div className="buttons">
                        <button type='submit'>Submit</button>
                        <button type='button'
                        onClick={this.onCancel}>Cancel</button>
                    </div>
                </form>
            </div>
        )
    }

    render() {
        const isShown = this.props.isShown;
        return isShown && !this.props.loading ? this.getEditForm() : null;
    }
}

const mapStateToProps = state => {
    return {
        messages: state.data.messages,
        isShown: state.editForm.isShown,
        messageId: state.editForm.messageId,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        editMessage: (id, text) => dispatch(editMessage(id, text)),
        hidePage: () => dispatch(hidePage()),
        dropCurrentMessageId: () => dispatch(dropCurrentMessageId())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (EditForm);