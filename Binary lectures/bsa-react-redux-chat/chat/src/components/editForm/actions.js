import * as types from './actionTypes'

export const showPage = () => ({
    type: types.SHOW_PAGE
});

export const hidePage = () => ({
    type: types.HIDE_PAGE
})

export const setCurrentMessageId = (id) => ({
    type: types.SET_CURRENT_MESSAGE_ID,
    payload: { id }
})

export const dropCurrentMessageId = () => ({
    type: types.DROP_CURRENT_MESSAGE_ID
})