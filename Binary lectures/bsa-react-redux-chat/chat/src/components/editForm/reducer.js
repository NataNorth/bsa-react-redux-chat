import * as types from './actionTypes'

const initialState = {
    messageId: '',
    isShown: false
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case types.SET_CURRENT_MESSAGE_ID: {
            return {
                ...state,
                messageId: action.payload.id
            };
        }

        case types.DROP_CURRENT_MESSAGE_ID: {
            return {
                ...state,
                messageId: ''
            }
        }

        case types.SHOW_PAGE: {
            return {
                ...state,
                isShown: true
            }
        }

        case types.HIDE_PAGE: {
            return {
                ...state,
                isShown: false
            }
        }

        default:
            return state;
        
    }
}