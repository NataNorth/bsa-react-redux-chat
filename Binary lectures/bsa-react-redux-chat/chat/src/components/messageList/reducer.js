import * as types from './ActionTypes'
  
  const initialState = {
    messages: [],
    loading: true,
    currentUser: {},
    error: null
  };
  
  export default function messagesReducer(state = initialState, action) {
    switch(action.type) {
      case types.FETCH_MESSAGES_BEGIN:
        return {
          ...state,
          loading: true,
          error: null
        };
  
      case types.FETCH_MESSAGES_SUCCESS: {
        const { messages, currentUser} = action.payload;
        let currUserMessages = messages.filter(message => {return message.userId === currentUser.id});
        let message =  currUserMessages[currUserMessages.length - 1];
        return {
          ...state,
          loading: false,
          messages: messages,
          currentUser: {
            ...currentUser,
            lastMessageId: message.id
          }
        };
      }
  
      case types.FETCH_MESSAGES_FAILURE:
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          messages: []
        };
      
      case types.ADD_MESSAGE: {
        const { id, text, createdAt } = action.payload;
        var newMessage = {
          id: id,
          text: text,
          user: state.currentUser.name,
          avatar: state.currentUser.avatar,
          userId: state.currentUser.id,
          editedAt: "",
          createdAt: createdAt
        }
        return {
          ...state,
          messages: [...state.messages, newMessage],
          currentUser: {
            ...state.currentUser,
            lastMessageId: id
          }
        };
      }

      case types.DELETE_MESSAGE: {
        var newMessages = state.messages.filter(message => message.id !== action.payload.id);
        let messageId = '';
        if (action.payload.id === state.currentUser.lastMessageId) {
          let currUserMessages = newMessages.filter(message => message.userId === state.currentUser.id);
          messageId =  currUserMessages[currUserMessages.length - 1].id;
        }
          return {
            ...state,
            messages: newMessages,
            currentUser: {
              ...state.currentUser,
              lastMessageId: messageId
            }
          };
      }

      case types.EDIT_MESSAGE: {
        const { id, text, editedAt } = action.payload;
        var editedMessages = state.messages.map(message => {
          if (message.id === id) {
            return {
              ...message,
              text: text, 
              editedAt: editedAt
            };
          } else {
            return message;
          } });
          return {
            ...state,
            messages: editedMessages
          }
      }

      default:
        return state;
    }
  }