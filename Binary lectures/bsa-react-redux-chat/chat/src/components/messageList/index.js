import React from 'react';
import Message from './message/index'
import Separator from './Separator'
import './index.css'

import { connect } from "react-redux";
import { fetchMessages } from "./actions";


class MessageList extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchMessages());
    }
    
    render() {
        var previousDate = '';
      return ( this.props.loading ? 
        <div className="ui active centered massive loader"></div> :
        <div className ="messageList"> 
            {this.props.messages.map(message => {
                const currDate = message.createdAt.split('T')[0];
                var element = '';
                if (previousDate && previousDate !== currDate ) {
                    element =
                        <div>
                            <Separator date = {previousDate}/>
                            <Message message = {message} key={message.id}/>
                        </div>
                } else {
                    element =
                        <Message message = {message} key={message.id}/>
                }
                previousDate = currDate;
                return (element)
            })}              
       </div>
      )
    }
}

const mapStateToProps = state => ({
    messages: state.data.messages,
    loading: state.data.loading
});

export default connect(mapStateToProps)(MessageList);