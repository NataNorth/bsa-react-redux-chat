import * as types from './ActionTypes'
import service from '../chat/service';

export function fetchMessages() {
    return dispatch => {
      dispatch(fetchMessagesBegin());
      return fetch("https://api.npoint.io/21df9f7846c2077083ab")
        .then(handleErrors)
        .then(response => response.json())
        .then(data => {
            var sortedData = data.sort((a,b) => (a.createdAt > b.createdAt) ? 1 : ((b.createdAt > a.createdAt) ? -1 : 0));
            dispatch(fetchMessagesSuccess(sortedData));
            return sortedData;
        })
        .catch(error => dispatch(fetchMessagesFailure(error)));
    };
  }
  
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }

  export const fetchMessagesBegin = () => ({
    type: types.FETCH_MESSAGES_BEGIN
  });
  
  export const fetchMessagesSuccess = messages => ({
    type: types.FETCH_MESSAGES_SUCCESS,
    payload: { 
      messages,
      currentUser: {
        id: messages[0].userId,
        name: messages[0].user,
        avatar: messages[0].avatar
      } 
     }
  });
  
  export const fetchMessagesFailure = error => ({
    type: types.FETCH_MESSAGES_FAILURE,
    payload: { error }
  });

  export const addMessage = text => ({
    type: types.ADD_MESSAGE,
    payload: { 
      id: service.getNewId(),
      createdAt: service.getTime(),
      text 
    }
  });

  export const deleteMessage = id => ({
    type: types.DELETE_MESSAGE,
    payload: { id }
  });

  export const editMessage = (id, text) => ({
    type: types.EDIT_MESSAGE,
    payload: { 
      id, text,
      editedAt: service.getTime()
     }
  });
