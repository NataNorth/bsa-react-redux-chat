import React from 'react';
import './App.css';
import MessageList from '../messageList/index'
import MessageInput from '../messageInput/index'
import Header from '../header/index'
import EditForm from '../editForm/index'

function App() {
  return (
    <div className="chat">  
      <Header />
      <MessageList />
      <EditForm />
      <MessageInput />
    </div>
);
}

export default App;
