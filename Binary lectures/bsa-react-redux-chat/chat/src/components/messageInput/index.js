import React from 'react';
import './index.css'
import { connect } from 'react-redux';
import { addMessage, editMessage } from '../messageList/actions';

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultMessage();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    componentDidMount() {
        document.addEventListener('keyup', this.handleKeyPress, true);
    }

    componentWillUnmount() {
        document.removeEventListener('keyup', this.handleKeyPress, true);
    }

    getDefaultMessage() {
        return {
            text: '',
            edit: false
        };
    }

    handleChange(e) {
        this.setState({
                text: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault()
        if (this.state.text === '') {
            return;
        }
        if (this.state.edit) {
            this.props.editMessage(this.props.lastMessageId, this.state.text);
            this.setState({
                edit: false
            })
        } else {
            this.props.addMessage(this.state.text);
        }
        this.setState(this.getDefaultMessage())
    }

    handleKeyPress(e) {
        if (e.key === 'ArrowUp') {
            console.log('m ' + this.props.lastMessageId);
            let text = this.props.messages.find(message => message.id === this.props.lastMessageId).text;
            document.getElementById('enterMessageTextarea').value = text;
            this.setState({
                edit: true,
                text: text
            })
        }
    }

    render() {
        return (
            <form
                onSubmit={this.handleSubmit}
                className="messageInput">
                <textarea
                    id='enterMessageTextarea'
                    onChange={this.handleChange}
                    onKeyPress={this.handleKeyPress}
                    value={this.state.text}
                    placeholder="Message" />
                <button
                    type='submit'
                >Send</button>
            </form>
        )
    }
  }

const mapStateToProps = state => ({
    lastMessageId: state.data.currentUser.lastMessageId,
    messages: state.data.messages
})

const mapDispatchToProps = dispatch => {
    return {
      addMessage: text => dispatch(addMessage(text)),
      editMessage: (id, text) => dispatch(editMessage(id, text))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);