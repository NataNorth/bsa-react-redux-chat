import { createStore, applyMiddleware, compose } from '@reduxjs/toolkit';
import chat from '../reducers/chat'
import thunk from "redux-thunk";

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
    chat,
    composeEnhancer(applyMiddleware(thunk))
);
